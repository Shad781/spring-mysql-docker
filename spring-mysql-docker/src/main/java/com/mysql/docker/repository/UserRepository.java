package com.mysql.docker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mysql.docker.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
}
