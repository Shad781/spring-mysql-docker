package com.mysql.docker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.docker.entity.User;
import com.mysql.docker.repository.UserRepository;



@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserRepository repository;
	
	@GetMapping("/")
	public List<User> gethello() {
		return repository.findAll();
	}
	
	@PostMapping("/")
	public String postuser(@RequestBody User user) {
		repository.save(user);
		return "User Saved";
	}
	
	@GetMapping("/hello")
	public String helo() {
		return "Hello world";
	}
}
